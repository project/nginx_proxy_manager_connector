# Nginx Proxy Manager Connector

## Introduction
The Nginx Proxy Manager Connector module provides integration with a Nginx Proxy Manager instance.

Nginx Proxy manager helps in configuring proxy and certification settings for domains.

This module aims to provide service(s) to manage a nginx proxy manager instance using APIs.

## Prerequisites
- A running Nginx Proxy Manager instance

## Features
The module currently provides the following features via functions in the service:

- Create Proxy Host with/without certificate
- Update certificate of a Proxy Host
- Enable/Deploy Proxy Host
- Delete Proxy Host
- Get Proxy Host ID
- Get Certificate of a Proxy Host
- Get Certificate ID

Project homepage: https://www.drupal.org/project/nginx_proxy_manager_connector


## Dependencies
None

## Installation
1. Install the [latest release](https://www.drupal.org/project/nginx_proxy_manager_connector/releases/) of the module
1. Open the configuration page, using the link in Module list
   ![Screenshot of module list page showing configuration link](https://www.drupal.org/files/issues/2023-05-08/nginx_proxy_manager_connector%20module%20list.png)
1. Complete the settings form

## Maintainers:
* [Shashikanth Palvatla (shashikanth171)](https://www.drupal.org/u/shashikanth171)


Adding ToDo items
* Add documentation for setting up local Nginx Proxy manager instance
* Recommended version for Nginx Proxy Manager.