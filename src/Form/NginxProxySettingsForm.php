<?php

namespace Drupal\nginx_proxy_manager_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\nginx_proxy_manager_connector\Services\NginxProxyManagerService;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Class NginxProxySettingsForm.
 */
class NginxProxySettingsForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Nginx Proxy Manager Service.
   *
   * @var \Drupal\boutpets_nginxproxymanager\Services\NginxProxyManagerService
   */
  protected $nginxProxyManagerService;

  /**
   * Class Constructor.
   *
   * @param \Drupal\boutpets_nginxproxymanager\Services\NginxProxyManagerService $nginx_proxy_manager_service
   *   Nginx Proxy Manager Service.
   */
  public function __construct(NginxProxyManagerService $nginx_proxy_manager_service) {
    $this->nginxProxyManagerService = $nginx_proxy_manager_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
          $container->get('nginx_proxy_manager_connector.nginx_proxy_manager_service')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nginx_proxy_manager_connector.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nginx_proxy_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nginx_proxy_manager_connector.settings');
    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('1. API Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['api_settings']['api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#description' => $this->t('Enter the base url for the API calls to Nginx Proxy Manager instance.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required'=> TRUE,
      '#default_value' => $config->get('api_base_url'),
    ];

    $form['api_settings']['email_nginx_proxy'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('User credentials of Nginx Proxy Manager.'),
      '#disabled' => FALSE,
      '#maxlength' => 64,
      '#size' => 64,
      '#required'=> TRUE,
      '#default_value' => $config->get('email_nginx_proxy'),
    ];

    $form['api_settings']['password_nginx_proxy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('User credentials of Nginx Proxy Manager.'),
      '#disabled' => FALSE,
      '#maxlength' => 64,
      '#size' => 64,
      '#required'=> TRUE,
      '#default_value' => $config->get('password_nginx_proxy'),
    ];
    $form['api_settings']['test_connection'] = [
      '#type' => 'checkboxes',
      '#options' => ['test_api' => $this->t('Validate Connection')],
      '#title' => $this->t('Validate Connection'),
      '#default_value' => $config->get('test_connection', ['test_api']) ?? [],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'validateApiCallback'],
        'wrapper' => 'validate-container',
        'effect' => 'none',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying API Base URL...'),
        ],
      ],
    ];
    $form['api_settings']['container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="validate-container">',
      '#suffix' => '</div>',
    ];

    $form['default_proxy_host_settings '] = [
      '#type' => 'fieldset',
      '#title' => $this->t('2. Default Proxy Host Settings '),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['default_proxy_host_settings ']['default_master_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Master Domain'),
      '#description' => $this->t('Enter the default master domain for proxy host.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('default_master_domain'),
    ];
    $form['default_proxy_host_settings ']['default_forward_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Forward Host'),
      '#description' => $this->t('Enter the default forward host to be used for the proxy hosts created by the module.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('default_forward_host'),
      '#disabled' => FALSE,
    ];
    $form['default_proxy_host_settings ']['default_forward_host_port'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Forward Host Port'),
      '#description' => $this->t('Enter the default forward host port to be used.'),
      '#maxlength' => 10,
      '#size' => 10,
      '#default_value' => $config->get('default_forward_host_port'),
    ];

    $form['certificates_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('3. Certificates'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['certificates_settings']['letsencrypt_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email for generating LetsEncrypt Certificates'),
      '#description' => $this->t('Enter the email used for managing LetsEncrypt Certificates.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('letsencrypt_email'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->configFactory->getEditable('nginx_proxy_manager_connector.settings')
      ->set('api_base_url', $form_state->getValue('api_base_url'))
      ->set('default_forward_host', $form_state->getValue('default_forward_host'))
      ->set('default_forward_host_port', $form_state->getValue('default_forward_host_port'))
      ->set('default_master_domain', $form_state->getValue('default_master_domain'))
      ->set('letsencrypt_email', $form_state->getValue('letsencrypt_email'))
      ->set('password_nginx_proxy', $form_state->getValue('password_nginx_proxy'))
      ->set('email_nginx_proxy', $form_state->getValue('email_nginx_proxy'))
      ->set('test_connection', $form_state->getValue('test_connection'))
      ->save();

  }

  /**
   * Ajax callback for a validate the API URL.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form builder array.
   */
  public function validateApiCallback(array &$form, FormStateInterface $form_state)
  {
    $current_api_url = $form_state->getValue('api_base_url');
    $email_nginx_proxy = $form_state->getValue('email_nginx_proxy');
    $pass_nginx_proxy = $form_state->getValue ('password_nginx_proxy');


    if ($form_state->getValue('test_connection')['test_api'] == 'test_api') {

      if (Json::decode($this->nginxProxyManagerService->apiCallHealthStatus($current_api_url))['status'] == 'OK') {
        if (isset(Json::decode($this->nginxProxyManagerService->apiCallGenerateToken($current_api_url, $email_nginx_proxy, $pass_nginx_proxy))['token'])) {
          $output = $this->t('Nginx Proxy Manager Connector is Connected');
          $class = ['success'];
        } else {
          $output = $this->t('Please check Nginx Proxy Manager credentials');
          $class = ['fail'];
        }
      } else {
        $output = $this->t('Please check Nginx Proxy Manager Api Base url');
        $class = ['fail'];
      }

      $response = new AjaxResponse();
      $response->addCommand(new HtmlCommand('#validate-container', $output));
      $response->addCommand(new InvokeCommand('#validate-container', 'removeClass', []));
      $response->addCommand(new InvokeCommand('#validate-container', 'addClass', $class));

      return $response;
    } else {
        $response = new AjaxResponse();
        $output = '';
        $response->addCommand(new HtmlCommand('#validate-container', $output));
        return $response;
    }

  }

}
