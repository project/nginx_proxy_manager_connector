<?php

namespace Drupal\nginx_proxy_manager_connector\Services;

use GuzzleHttp\ClientInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Service to integrate Nginx Proxy Manager.
 */
class NginxProxyManagerService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Drupal Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructor for NginxProxyManagerService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory for retrieving required config objects.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle client object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The Drupal Logger Factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $httpClient, LoggerChannelFactoryInterface $loggerFactory) {
    $this->httpClient = $httpClient;
    $this->config = $configFactory->get('nginx_proxy_manager_connector.settings');
    $this->api_calls_base_url = $this->config->get('api_base_url');
    $this->default_master_domain = $this->config->get('default_master_domain');
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Creates a Proxy Host in NginxProxyManager.
   *
   * @param string $custom_domain
   *   The custom domain for the proxy host.
   * @param string $custom_subdomain
   *   The subdomain for the proxy host.
   * @param string|null $forward_host
   *   The forward host for the proxy host.
   * @param string|null $port
   *   The port number for the proxy host.
   * @param string|null $letsencrypt_email
   *   The email for generating certificate for subdomain and custom domain.
   *
   * @return array
   *   The success or failure response.
   */
  public function createProxyHost($custom_domain, $custom_subdomain, $forward_host = NULL, $port = NULL, $letsencrypt_email = NULL) {

    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Proxy host not created.'];
    }

    // Set Defaults.
    if ($forward_host == NULL) {
      $forward_host = $this->config->get('default_forward_host');
    }
    if ($port == NULL) {
      $port = $this->config->get('default_forward_host_port');
    }
    if ($letsencrypt_email == NULL) {
      $letsencrypt_email = $this->config->get('letsencrypt_email');
    }

    $error_message = '';

    // Create domain names string.
    if ($custom_domain !== '') {
      $non_www_domain_name = preg_replace('/^www\./', '', $custom_domain);
      $www_domain_name = 'www.' . $non_www_domain_name;
      $custom_domain_domain_names = '"' . $non_www_domain_name . '", "' . $www_domain_name . '"';

      $proxy_host_id = $this->getProxyHostId($non_www_domain_name);

      if ($proxy_host_id == 0) {
        // Create Proxy Host for custom domain.
        $api_response_custom_domain = Json::decode($this->apiCallCreateProxyHostWithoutCertificate($custom_domain_domain_names, $forward_host, $port));
      }
      else {
        $api_response_custom_domain = Json::decode($this->apiCallEnableProxyHost($proxy_host_id));
      }
      // If error, send appropriate failure response.
      // Show subdomain error, if both throw error.
      if (isset($api_response_custom_domain['error'])) {
        $error_message = $error_message . $api_response_custom_domain['error']['message'];
        $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $api_response_custom_domain['error']['code'] . ", Message:" . $api_response_custom_domain['error']['message']);
      }
    }

    if ($this->getProxyHostId($custom_subdomain) == 0) {

      $custom_subdomain_domain_names = '"' . $custom_subdomain . '"';

      // Get certificate for custom subdomain and create if needed.
      $this->apiCallCreateCertificate($custom_subdomain, $letsencrypt_email);

      $certificate_id = $this->getCertificateId($custom_subdomain);

      // Create Proxy Host for custom subdomain.
      $api_response_custom_subdomain = Json::decode($this->apiCallCreateProxyHost($custom_subdomain_domain_names, $forward_host, $certificate_id, $port));
      // If error, send appropriate failure response.
      // Show subdomain error, if both throw error.
      if (isset($api_response_custom_subdomain['error'])) {
        $error_message = $error_message . $api_response_custom_subdomain['error']['message'];
        $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $api_response_custom_subdomain['error']['code'] . ", Message:" . $api_response_custom_subdomain['error']['message']);
      }
    }

    if ($error_message !== '') {
      $response = ['failure' => $error_message];
    }
    else {
      $response = ['success' => TRUE];
    }

    return $response;
  }

  /**
   * Deploy Proxy Host in NginxProxyManager.
   *
   * @param string $custom_domain
   *   The custom domain of the proxy host.
   * @param int $proxy_host_id
   *   The proxy host id which uses the custom domain.
   *
   * @return int|string
   *   The success or failure response.
   */
  public function deployProxyHost($custom_domain, $proxy_host_id = 0) {

    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Proxy host not enabled.'];
    }

    // If Proxy Host Id is not provided find it by custom_domain.
    if ($proxy_host_id == 0) {
      $proxy_host_id = $this->getProxyHostId($custom_domain);
    }
    $response = Json::decode($this->apiCallEnableProxyHost($proxy_host_id));
    if (isset($response['error'])) {
      $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $response['error']['code'] . ", Message:" . $response['error']['message']);
    }
    return $response;
  }

  /**
   * Disable Proxy Host in NginxProxyManager.
   *
   * @param string $custom_domain
   *   The custom domain of the proxy host.
   * @param int $proxy_host_id
   *   The proxy host id which uses the custom domain.
   *
   * @return bool|array
   *   The success or failure response.
   */
  public function disableProxyHost($custom_domain, $proxy_host_id = 0) {
    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Proxy host not disabled.'];
    }

    // If Proxy Host Id is not provided find it by custom_domain.
    if ($proxy_host_id == 0) {
      $proxy_host_id = $this->getProxyHostId($custom_domain);
    }
    $response = Json::decode($this->apiCallDisableProxyHost($proxy_host_id));
    if (isset($response['error'])) {
      $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $response['error']['code'] . ", Message:" . $response['error']['message']);
    }
    return $response;
  }

  /**
   * Update Proxy Host in NginxProxyManager.
   *
   * @param string $old_custom_domain
   *   The old custom domain of the proxy host to be replaced.
   * @param string $new_custom_domain
   *   The new custom domain of the proxy host to be added.
   * @param int $proxy_host_id
   *   The current proxy host ID.
   * @param int $enable_ssl
   *   The SSH status of the proxy host to be set.
   *
   * @return array
   *   The success or failure response.
   */
  public function updateProxyHost($old_custom_domain, $new_custom_domain, $proxy_host_id = 0, $enable_ssl = 0) {

    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Proxy host not updated.'];
    }

    // If Proxy Host Id is not provided find it by custom_domain.
    if ($old_custom_domain !== "" && $proxy_host_id == 0) {
      $proxy_host_id = $this->getProxyHostId($old_custom_domain);
    }

    // Set domain names.
    $non_www_domain_name = preg_replace('/^www\./', '', $new_custom_domain);
    $www_domain_name = 'www.' . $non_www_domain_name;
    $custom_domain_domain_names = '"' . $non_www_domain_name . '", "' . $www_domain_name . '"';

    if ($proxy_host_id == 0) {
      // Set Defaults.
      $config = \Drupal::config('nginx_proxy_manager_connector.settings');
      $forward_host = $this->config->get('default_forward_host');
      $port = $config->get('default_forward_host_port');
      // Create Proxy Host for custom domain.
      $response = Json::decode($this->apiCallCreateProxyHostWithoutCertificate($custom_domain_domain_names, $forward_host, $port));
    }
    else {
      $response = Json::decode($this->apiCallUpdateProxyHost($custom_domain_domain_names, $proxy_host_id));
    }

    if ($enable_ssl) {
      $cert_response = $this->updateProxyHostCertificate($new_custom_domain);
      if (isset($cert_response['error'])) {
        $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $response['error']['code'] . ", Message:" . $response['error']['message']);
      }
    }

    if (isset($response['error'])) {
      $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $response['error']['code'] . ", Message:" . $response['error']['message']);
    }
    return $response;
  }

  /**
   * Update the certificate for a proxy host in NginxProxyManager.
   *
   * @param string $custom_domain
   *   The custom domain for the proxy host.
   * @param string $action
   *   (optional) The action to perform. Default is "add".
   * @param string|null $letsencrypt_email
   *   (optional) The email address for Let's Encrypt.
   * @param int $proxy_host_id
   *   (optional) The ID of the proxy host to update.
   *
   * @return array
   *   An array with a success/failure message.
   */
  public function updateProxyHostCertificate($custom_domain, $action = "add", $letsencrypt_email = NULL, $proxy_host_id = 0) {

    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Proxy host not updated.'];
    }
    if ($custom_domain == '') {
      return ['failure' => 'Empty custom domain'];
    }
    // Set De$config = \Drupal::config('nginx_proxy_manager_connector.nginxproxysettings');faults.
    $config = \Drupal::config('nginx_proxy_manager_connector.settings');
    if ($letsencrypt_email == NULL) {
      $letsencrypt_email = $config->get('letsencrypt_email');
    }
    if ($action == "add") {

      // Check if certificate exists.
      $certificate_id_from_list = $this->getCertificateId($custom_domain);
      // Create if it doesn't exist.
      if ($certificate_id_from_list == 0) {
        // Create certificate.
        $this->apiCallCreateCertificate($custom_domain, $letsencrypt_email);
        $certificate_id_from_list = $this->getCertificateId($custom_domain);

        if ($certificate_id_from_list == 0) {
          return ["failure" => "Certificate not generated"];
        }
      }
      $certificate_id = $certificate_id_from_list;
    }
    elseif ($action = "remove") {
      $certificate_id = 0;
    }

    // If Proxy Host Id is not provided find it by custom_domain.
    if ($proxy_host_id == 0) {
      $proxy_host_id = $this->getProxyHostId($custom_domain);
    }

    $response = Json::decode($this->apiCallUpdateProxyHostCertificate($certificate_id, $proxy_host_id));
    if (isset($response['error'])) {
      $this->loggerFactory->get('nginx_proxy_manager_connector')->error("Nginx Proxy Manager Error, Code:" . $response['error']['code'] . ", Message:" . $response['error']['message']);
    }
    return $response;
  }

  /**
   * Get the ID of a proxy host using its domain name.
   *
   * @param string $custom_domain
   *   The custom domain of the proxy host.
   *
   * @return int
   *   The ID of the proxy host, or 0 if not found.
   */
  public function getProxyHostId($custom_domain) {
    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Nginx Proxy Manager instance is offline.'];
    }

    // Get list of proxy hosts.
    $proxy_hosts_list = Json::decode($this->apiCallGetProxyHosts());

    foreach ($proxy_hosts_list as $proxy_host) {
      if (in_array($custom_domain, $proxy_host['domain_names'])) {
        return $proxy_host['id'];
      }
    }
    return 0;
  }

  /**
   * Get Proxy Host Id using domain name.
   *
   * @param string $custom_domain
   *   The custom domain of a certificate.
   *
   * @return int|array
   *   If successful, the certificate ID. If unsuccessful, an array with a "failure" key.
   */
  public function getProxyHostCertificate($custom_domain) {
    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Nginx Proxy Manager instance is offline.'];
    }

    // Get list of proxy hosts.
    $proxy_hosts_list = Json::decode($this->apiCallGetProxyHosts());

    foreach ($proxy_hosts_list as $proxy_host) {
      if (in_array($custom_domain, $proxy_host['domain_names'])) {
        return $proxy_host['certificate_id'];
      }
    }
    return 0;
  }

  /**
   * Get Certificate Id using domain name.
   *
   * @param string $custom_domain
   *   The custom domain of a certificate.
   *
   * @return int
   *   The response return the certificate ID.
   */
  public function getCertificateId($custom_domain) {
    // Initiate API calls configuration.
    if (!$this->apiCallInit()) {
      return ['failure' => 'Nginx Proxy Manager instance is offline.'];
    }

    // Get list of proxy hosts.
    $certificates_list = Json::decode($this->apiCallGetCertificates());

    foreach ($certificates_list as $certificate) {
      if (in_array($custom_domain, $certificate['domain_names'])) {
        return $certificate['id'];
      }
    }

    return 0;
  }

  // ----------------------- API Calls ------------------------------

  /**
   * Check Nginx Proxy Manager status and generate token.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/tokens.json#L35
   *
   * @return bool
   *   Returns TRUE if the API call sets the API token in the config form, otherwise FALSE.
   */
  public function apiCallInit() {

    // Check if Nginx Proxy Manager is online.
    if (Json::decode($this->apiCallHealthStatus())['status'] == 'OK') {

      // Check if user credentials are configured.
      if ($this->config->get('email_nginx_proxy') !== NULL && $this->config->get('password_nginx_proxy') !== NULL) {

        // Generate token.
        if (isset(Json::decode($this->apiCallGenerateToken())['token'])) {
          \Drupal::state()->set('api_calls_token', Json::decode($this->apiCallGenerateToken())['token']);
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      return FALSE;
    }
    return FALSE;
  }

  /**
   * API call to check the status of Nginx Proxy Manager.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/67208e43cc41f659ae2e68baa58a181a4d5f465b/backend/doc/api.swagger.json#LL14C8-L14C8
   *
   * @return \Drupal\Component\Serialization\Json
   *   The response containing the health status to check if the API is returning data or not.
   */
  public function apiCallHealthStatus($api_url = '') {
    if ($api_url == '') {
      $api_url = $this->api_calls_base_url;
    }
    // Make request and return response.
    try {
      return $this->httpClient->request('GET', $api_url)->getBody()->getContents();

    }
    catch (\Exception $e) {
      $res = ['status' => 'FAIL'];
      return json_encode($res, JSON_FORCE_OBJECT);
    }
  }

  /**
   * API Call for generating a token for authentication.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/tokens.json#L35
   *
   * @return \Drupal\Component\Serialization\Json
   *   The response that contains the generated token.
   */
  public function apiCallGenerateToken($current_api_url='',$email_nginx_proxy ='',$pass_nginx_proxy ='') {

    if ($email_nginx_proxy =='' && $pass_nginx_proxy =='' && $current_api_url == '') {

      $email_nginx_proxy = $this->config->get('email_nginx_proxy');
      $pass_nginx_proxy = $this->config->get('password_nginx_proxy');
      $current_api_url = $this->api_calls_base_url;

    }


    /// / Set data.
    $data = [
      'identity' => $email_nginx_proxy,
      'scope' => 'user',
      'secret' => $pass_nginx_proxy,
    ];

      // Make request.
    try {
      $response_json = $this->httpClient->request(
        'POST',
        $current_api_url . 'tokens',
        [
          'body' => json_encode($data, JSON_FORCE_OBJECT),
          'headers' => [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
          ],
        ]
      )->getBody()->getContents();

      // Return response.

    return $response_json;
    }
    catch (\Exception $e) {
      $res = ['status' => 'FAIL'];
       return json_encode($res, JSON_FORCE_OBJECT);
    }

  }

  /**
   * API Call for creating a proxy host.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#LL193C11-L193C11
   *
   * @param string $domain_names
   *   The domain name(s) for a proxy host.
   * @param string $forward_host
   *   The forward host for a proxy host.
   * @param int $certificate_id
   *   The certificate ID for a proxy host.
   * @param string $port
   *   The port number for a proxy host.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The response that contains the domain details after creating a record on the proxy manager.
   */
  public function apiCallCreateProxyHost($domain_names, $forward_host, $certificate_id, $port) {

    // Set data.
    $data = [
      'domain_names' => "",
      'forward_scheme' => "https",
      'forward_host' => $forward_host,
      'forward_port' => $port,
      'certificate_id' => $certificate_id,
      'ssl_forced' => TRUE,
      'hsts_enabled' => TRUE,
      'hsts_subdomains' => FALSE,
      'http2_support' => FALSE,
      'block_exploits' => TRUE,
      'caching_enabled' => FALSE,
      'allow_websocket_upgrade' => FALSE,
      'access_list_id' => '0',
      'advanced_config' => "",
      'enabled' => TRUE,
    ];

    $data = str_replace('"domain_names":""', '"domain_names": [' . $domain_names . ']', json_encode($data, JSON_FORCE_OBJECT));

    // Make request.
    $response_json = $this->httpClient->request(
          'POST',
          $this->api_calls_base_url . 'nginx/proxy-hosts',
          [
            'body' => $data,
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'Accept' => 'application/json',
              'X-API-Version' => 'next',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

  /**
   * Creates a proxy host without certificate.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#LL193C11-L193C11
   *
   * @param string $domain_names
   *   The domain name(s) for a proxy host.
   * @param string $forward_host
   *   The forward host for a proxy host.
   * @param string $port
   *   The port number for a proxy host.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The response with details of the created record on the proxy manager.
   */
  public function apiCallCreateProxyHostWithoutCertificate($domain_names, $forward_host, $port) {

    // Set data.
    $data = [
      'domain_names' => "",
      'forward_scheme' => "https",
      'forward_host' => $forward_host,
      'forward_port' => $port,
      'certificate_id' => '0',
      'ssl_forced' => FALSE,
      'hsts_enabled' => FALSE,
      'hsts_subdomains' => FALSE,
      'http2_support' => FALSE,
      'block_exploits' => TRUE,
      'caching_enabled' => FALSE,
      'allow_websocket_upgrade' => FALSE,
      'access_list_id' => '0',
      'advanced_config' => "",
      'enabled' => TRUE,
    ];

    $data = str_replace('"domain_names":""', '"domain_names": [' . $domain_names . ']', json_encode($data, JSON_FORCE_OBJECT));

    // Make request.
    $response_json = $this->httpClient->request(
          'POST',
          $this->api_calls_base_url . 'nginx/proxy-hosts',
          [
            'body' => $data,
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'Accept' => 'application/json',
              'X-API-Version' => 'next',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

  /**
   * Retrieves proxy host details.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#L174
   *
   * @return \Drupal\Component\Serialization\Json
   *   The response containing proxy host details.
   */
  public function apiCallGetProxyHosts() {

    // Make request.
    $response_json = $this->httpClient->request(
          'GET',
          $this->api_calls_base_url . 'nginx/proxy-hosts',
          [
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'X-API-Version' => 'next',
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

  /**
   * Enables a proxy host.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#L272
   *
   * @param int $proxy_host_id
   *   The ID of the proxy host to be enabled.
   *
   * @return bool
   *   The success of failure response.
   */
  public function apiCallEnableProxyHost($proxy_host_id) {

    // Make request.
    $response_json = $this->httpClient->request(
          'POST',
          $this->api_calls_base_url . 'nginx/proxy-hosts/' . $proxy_host_id . '/enable',
          [
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'X-API-Version' => 'next',
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

  /**
   * Disables a proxy host.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#L373
   *
   * @param int $proxy_host_id
   *   The ID of the proxy host to be disabled.
   *
   * @return bool
   *   The success of failure response.
   */
  public function apiCallDisableProxyHost($proxy_host_id) {
    // Make request.
    $response_json = $this->httpClient->request(
          'POST',
          $this->api_calls_base_url . 'nginx/proxy-hosts/' . $proxy_host_id . '/disable',
          [
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'X-API-Version' => 'next',
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

  /**
   * Updates a proxy host.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#L272
   *
   * @param string $domain_names
   *   The domain names to be added.
   * @param int $proxy_host_id
   *   The ID of the proxy host to be updated.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The success of failure response.
   */
  public function apiCallUpdateProxyHost($domain_names, $proxy_host_id) {
    // Set data.
    $data = ["domain_names" => ""];
    $data = str_replace('"domain_names":""', '"domain_names": [' . $domain_names . ']', json_encode($data, JSON_FORCE_OBJECT));

    // Make request.
    $response_json = $this->httpClient->request(
          'PUT',
          $this->api_calls_base_url . 'nginx/proxy-hosts/' . $proxy_host_id,
          [
            'body' => $data,
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'Accept' => 'application/json',
              'X-API-Version' => 'next',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    return $response_json;
  }

  /**
   * Makes an API call to update the certificate for the given proxy host.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/proxy-hosts.json#L272
   *
   * @param int $certificate_id
   *   The ID of the certificate for proxy host.
   * @param int $proxy_host_id
   *   The ID of the proxy host for which the certificate should be updated.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The success of failure response.
   */
  public function apiCallUpdateProxyHostCertificate($certificate_id, $proxy_host_id) {

    // Set data.
    $data = [
      "certificate_id" => $certificate_id,
      'ssl_forced' => TRUE,
      'hsts_enabled' => TRUE,
    ];

    // Make request.
    $response_json = $this->httpClient->request(
          'PUT',
          $this->api_calls_base_url . 'nginx/proxy-hosts/' . $proxy_host_id,
          [
            'body' => json_encode($data, JSON_FORCE_OBJECT),
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'Accept' => 'application/json',
              'X-API-Version' => 'next',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();
    return $response_json;
  }

  /**
   * Makes an API call to create a new certificate for the given domain.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/certificates.json#L111
   *
   * @param string $custom_domain
   *   The custom domain name for which a certificate should be created.
   * @param string $letsencrypt_email
   *   The email address to use for Let's Encrypt registration.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The success of failure response.
   */
  public function apiCallCreateCertificate($custom_domain, $letsencrypt_email) {
    $non_www_domain_name = preg_replace('/^www\./', '', $custom_domain);
    $www_domain_name = 'www.' . $non_www_domain_name;

    $domain_names = '"' . $non_www_domain_name . '", "' . $www_domain_name . '"';

    // Set data.
    $data = [
      "domain_names" => "",
      "provider" => "letsencrypt",
      "nice_name" => "test",
      "meta" => [
        "letsencrypt_email" => $letsencrypt_email,
        "letsencrypt_agree" => TRUE,
        "dns_challenge" => FALSE,
      ],
    ];

    $data = str_replace('"domain_names":""', '"domain_names": [' . $domain_names . ']', json_encode($data, JSON_FORCE_OBJECT));

    // Make request.
    $response_json = $this->httpClient->request(
          'POST',
          $this->api_calls_base_url . 'nginx/certificates',
          [
            'body' => $data,
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'Accept' => 'application/json',
              'X-API-Version' => 'next',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    return $response_json;

  }

  /**
   * Gets list of all certificates.
   * Uses endpoint https://github.com/NginxProxyManager/nginx-proxy-manager/blob/86ddd9c83cbd1598d7b62bbea6d691fbe658dfc9/backend/schema/endpoints/certificates.json#LL92C8-L92C8
   *
   * @return \Drupal\Component\Serialization\Json
   *   The success of failure response.
   */
  public function apiCallGetCertificates() {
    // Make request.
    $response_json = $this->httpClient->request(
          'GET',
          $this->api_calls_base_url . 'nginx/certificates',
          [
            'headers' => [
              'Authorization' => 'Bearer ' . \Drupal::state()->get('api_calls_token'),
              'X-API-Version' => 'next',
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
            ],
          ]
      )->getBody()->getContents();

    // Return response.
    return $response_json;
  }

}
